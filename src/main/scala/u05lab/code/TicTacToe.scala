package u05lab.code

object TicTacToe extends App {
    
    sealed trait Player {
        def other: Player = this match {
            case X() => O();
            case _ => X()
        }
        
        override def toString: String = this match {
            case X() => "X";
            case _ => "O"
        }
    }
    
    case class X() extends Player
    
    case class O() extends Player
    
    /**
      *
      * @param x horizontal position of the Mark
      * @param y vertical position of the Mark
      * @param player Type of the Mark: Circle or Cross
      */
    case class Mark(x: Double, y: Double, player: Player)
    
    type Board = List[Mark]
    type Game = List[Board]
    
    /**
      * Find the Player that placed the Mark in position (x, y)
      *
      * @param board The Board of this Game
      * @param x The Horizontal position of the Mark
      * @param y The Vertical position of the Mark
      * @return The Player (Circle or Cross) that placed the Mark
      */
    def find(board: Board, x: Double, y: Double): Option[Player] = board match{
        case h::t => if (h.x == x && h.y == y) Some(h.player) else find(t, x, y)
        case _ => None
    }
    
    /**
      * Place the all the possible Marks for the given Player inside the given Board
      *
      * @param board The board of already placed Marks
      * @param player The Player of the Mark to be placed
      * @return A Sequence of Boards
      */
    def placeAnyMark(board: Board, player: Player): Seq[Board] = {
        
        var l:List[Board] = List()

        for (i <- 0 to 2){
            for (j <- 0 to 2){
                l = if(find(board, i, j).isEmpty) l ::: List(Mark(i, j, player) :: board) else l ::: List(board)
            }
        }
        
        l
    }
    
    def computeAnyGame(player: Player, moves: Int): Stream[Game] = ???
    
    def printBoards(game: Seq[Board]): Unit =
        for (y <- 0 to 2; board <- game.reverse; x <- 0 to 2) {
            print(find(board, x, y) map (_.toString) getOrElse ".")
            if (x == 2) {
                print(" "); if (board == game.head) println()
            }
        }
    
    // Exercise 1: implement find such that..
    println(find(List(Mark(0, 0, X())), 0, 0)) // Some(X)
    println(find(List(Mark(0, 0, X()), Mark(0, 1, O()), Mark(0, 2, X())), 0, 1)) // Some(O)
    println(find(List(Mark(0, 0, X()), Mark(0, 1, O()), Mark(0, 2, X())), 1, 1)) // None
    
    // Exercise 2: implement placeAnyMark such that..
    printBoards(placeAnyMark(List(), X()))
    //... ... ..X ... ... .X. ... ... X..
    //... ..X ... ... .X. ... ... X.. ...
    //..X ... ... .X. ... ... X.. ... ...
    
    printBoards(placeAnyMark(List(Mark(0, 0, O())), X()))
    //O.. O.. O.X O.. O.. OX. O.. O..
    //... ..X ... ... .X. ... ... X..
    //..X ... ... .X. ... ... X.. ...
    
    // Exercise 3 (ADVANCED!): implement computeAnyGame such that..
    computeAnyGame(O(), 4) foreach { g => printBoards(g); println() }
    //... X.. X.. X.. XO.
    //... ... O.. O.. O..
    //... ... ... X.. X..
    //              ... computes many such games (they should be 9*8*7*6 ~ 3000).. also, e.g.:
    //
    //... ... .O. XO. XOO
    //... ... ... ... ...
    //... .X. .X. .X. .X.
    
    // Exercise 4 (VERY ADVANCED!) -- modify the above one so as to stop each game when someone won!!
}