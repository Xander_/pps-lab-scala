package u04lab.code

/**
  * Created by Xander_C on 21/03/2017.
  */


// A person with immutable name/surname, mutable married flag, and toString
trait Person {
    
    def name: String
    def surname: String
    def married: Boolean
    def married_=(state: Boolean): Unit
    
    // a template method
    override def toString(): String = name + " " + surname + " " + married
    
    override def equals(p: scala.Any): Boolean = p match{
        
        case that: Person => that.name == this.name && that.surname == this.surname && that.married == this.married
        case _ => false
    }
}

// easiest implementation, with overriding class parameters becoming fields
class PersonImpl1(override val name: String,
                  override val surname: String,
                  override var married: Boolean) extends Person

// surname as a constant, mame with overridden getter
class PersonImpl2(_name: String,
                  var married: Boolean) extends Person {
    
    override def name: String = {
        require(_name != null) // may throw IllegalArgumentException
        _name
    }
    
    override val surname = "?"
}

// overridden getters and setters of married
class PersonImpl3(override val name: String,
                  override val surname: String,
                  private var _married: Boolean) extends Person {
    
    override def married: Boolean = {
        println("getting married property")
        _married
    }
    
    override def married_=(m: Boolean): Unit = {
        require (!_married || m)
        _married = m
    }
}

object Person{
    
    def apply(name: String, surname: String, married: Boolean) : Person = new PersonImpl3(name, surname, married)
}

// Note uniform access
object UsePerson extends App {
    
    val p: Person = new PersonImpl1("mario", "rossi", false)
    println(p)
    
    val p2: Person = new PersonImpl2("Luigi", false)
    println(p2)
    
    val p3: Person = new PersonImpl3("mario", "rossi", false)
    println(p3)
    
    p3.married = true
    println(p3)
    
    val p4: Person = Person("Gianni", "Degianno", false)
}
