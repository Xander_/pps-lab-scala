package u04lab.code

trait Complex {
    
    def re: Double
    
    def im: Double
    
    def +(c: Complex): Complex
    
    // should implement the sum of two complex numbers..
    def *(c: Complex): Complex // should implement the product of two complex numbers
    
    override def equals(c: scala.Any): Boolean = c match {
        case complex: Complex => this.re == complex.re && this.im == complex.im
        case _ => false
    }
    
    override def toString: String = {
        this.re.toString + "r + " + this.im.toString + "i"
    }
}


class ExtComplexImpl(override val re: Double,
                     override val im: Double) extends Complex {
    
    override def +(c: Complex): Complex =
        Complex(this.re + c.re, this.re + c.im)
    
    override def *(c: Complex): Complex =
        Complex(this.re * c.re - this.im * c.im, this.im * c.re + this.re * c.im)
}

object Complex {
    
    case class ComplexImpl(override val re: Double,
                           override val im: Double) extends Complex{
    
        override def +(c: Complex): Complex =
            Complex(this.re + c.re, this.re + c.im)
    
        override def *(c: Complex): Complex =
            Complex(this.re * c.re - this.im * c.im, this.im * c.re + this.re * c.im)
    }
    
    def apply(re: Double, im: Double): Complex  = ComplexImpl(re, im) // Fill here
}

object TryComplex extends App {
    val a = Array(Complex(10, 20), Complex(1, 1), Complex(7, 0))
    val c = a(0) + a(1) + a(2)
    println(c, c.re, c.im)
    // (ComplexImpl(18.0,21.0),18.0,21.0)
    val c2 = a(0) * a(1)
    println(c2, c2.re, c2.im) // (ComplexImpl(-10.0,30.0),-10.0,30.0)
    
    println(Complex(1.0, 0.0) == Complex(1.0, 0.0))
    println(Complex(0.0, 0.0))
}

/** Hints:
  * - implement Complex with a ComplexImpl class, similar to PersonImpl in slides
  * - check that equality and toString do not work
  * - use a case class ComplexImpl instead, creating objects without the 'new' keyword
  * - check equality and toString now
  */