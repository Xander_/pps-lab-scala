scalaSource in Compile := { ( baseDirectory in Compile )(_ / "src") }.value

scalaSource in Test := { ( baseDirectory in Compile )(_ / "test") }.value

val scalatest = "org.scalatest" % "scalatest_2 .12" % "3.0.1" % "test"

val tuprolog = "it.unibo.alice.tuprolog" % "tuprolog" % "2.1.1"

lazy val root = ( project in file (".")).settings (
    name := "pps-lab-scala-sbt", version := "1.0", organization := "unibo.pps",
    scalaVersion := "2.12.2",
    libraryDependencies ++= Seq ( tuprolog )
)
